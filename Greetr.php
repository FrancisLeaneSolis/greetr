<?php

namespace Solisleanesanity\Greetr;

class Greetr
{
    public function greet(string $sName)
    {
        return 'Hi ' . $sName . '! How are you doing today?';
    }
}
